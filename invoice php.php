<!DOCTYPE html>
<html>

<head>
    <title>Invoice</title>
    <style>
        /* CSS styling for the invoice */
        body {
            font-family: Arial, sans-serif;
            margin: 0 auto;
            padding: 40px;
            max-width: 2480px;
            width: 70%;
            background-color: white;
            box-sizing: border-box;
        }

        h1 {
            text-align: center;
            color: black;
        }

        h2 {
            color: black;
            margin-bottom: 10px;
            text-align: left;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 30px;
            text-align: left;
        }

        th,
        td {
            padding: 10px 15px;
            border-bottom: 1px solid #dddddd;
        }

        th {
            background-color: #22963f;
            color: white;
        }

        .total {
            margin-top: 20px;
        }

        .total span {
            font-weight: 600;
            font-size: 1.2rem;
            margin-left: 2rem;
        }

        .divider {
            margin: 1rem 0;
            border-bottom: 1px solid #dddddd;
        }

        .policy {
            margin-top: 30px;
            font-style: italic;
            color: #888;
        }

        .footer {
            margin-top: 20px;
            text-align: center;
        }

        .footer p {
            margin: 0;
            color: #999999;
        }

        @media print {

            html,
            body {
                width: 210mm;
                height: 297mm;
            }

            .page {
                margin: 0;
                border: initial;
                width: initial;
                min-height: initial;
                background: initial;
                page-break-after: always;
            }
        }
    </style>
</head>

<body>
    <h1>Invoice</h1>
    <h2>Information Customers</h2>
    <div class="booking-info">
        <table>
            <tr>
                <td><strong>Name</strong></td>
                <td>{{ $transaction->customers->name }}</td>
            </tr>
            <tr>
                <td><strong>Email</strong></td>
                <td>{{ $transaction->customers->email }}</td>
            </tr>
            <tr>
                <td><strong>Phone Number</strong></td>
                <td>{{ $transaction->customers->phone_number }}</td>
            </tr>
        </table>
    </div>

    <h2>Venue Details</h2>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Hotel Name</th>
                <th>Name</th>
                <th>Qty</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>{{ $transaction->hotels->nama }}</td>
                <td>{{ $transaction->wedding_venues->nama }}</td>
                <td>1</td>
                <td>@currency($transaction->venue_price)</td>
            </tr>
            @php
                $no = 1;
            @endphp
            @foreach ($transaction->transaction_rooms as $item)
                <tr>
                    <td colspan="2">{{ $no++ }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->qty }}</td>
                    <td>@currency($item->price)</td>
                </tr>
            @endforeach
            @if ($transaction->extra_bed)
                <tr>
                    <td>{{ $no }}</td>
                    <td>Extra Bed</td>
                    <td>{{ $transaction->extra_bed }}</td>
                    <td>@currency($transaction->extra_bed_total_price)</td>
                </tr>
            @endif
        </tbody>
    </table>

    <h2>Wedding Details</h2>
    <table>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Qty</th>
            <th>Price</th>
        </tr>
        <tr>
            @php
                $no = 1;
            @endphp
            @foreach ($transaction->transaction_package_weddings as $item)
                <td>{{ $no }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->qty }}</td>
                <td>@currency($item->price)</td>
            @endforeach
        </tr>
    </table>

    <h2>Additional Details</h2>
    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($transaction->transaction_entertainments as $item)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $item->name }}</td>
                    <td>@currency($item->price)</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="total">
        <h2>Total Price</h2>
        <div class="divider"></div>
        <span>@currency(100000000)</span>
        <div class="divider"></div>
    </div>

    <div class="policy">
        <h2>Policy</h2>
        @foreach ($transaction->transaction_policies as $item)
            <p>{{ $item->name }}</p>
        @endforeach
    </div>
    <div class="divider"></div>
    <div class="footer">
        <p>Thank you for your reservation. If you have any questions, please contact our support team.</p>
    </div>
</body>

</html>
